package com.formacion.practica.converters;

import com.formacion.practica.dto.RoleDto;
import com.formacion.practica.model.Role;
import org.springframework.core.convert.converter.Converter;

public class RoleConverter implements Converter<Role, RoleDto> {
    @Override
    public RoleDto convert(Role source) {
        RoleDto roleDto = new RoleDto();
        roleDto.setIdRole(source.getIdRole());
        roleDto.setRole(source.getRole());
        roleDto.setDescription(source.getDescription());
        return roleDto;
    }
}
