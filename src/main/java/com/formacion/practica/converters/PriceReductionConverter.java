package com.formacion.practica.converters;

import com.formacion.practica.dto.ItemDto;
import com.formacion.practica.dto.PriceReductionDto;
import com.formacion.practica.model.Item;
import com.formacion.practica.model.PriceReduction;
import org.springframework.core.convert.converter.Converter;

public class PriceReductionConverter implements Converter<PriceReduction, PriceReductionDto> {
    @Override
    public PriceReductionDto convert(PriceReduction source) {
        PriceReductionDto priceReductionDto = new PriceReductionDto();

        priceReductionDto.setIdPriceReduction(source.getIdPriceReduction());
        priceReductionDto.setReducedPrice(source.getReducedPrice());
        priceReductionDto.setStartDate(source.getStartDate());
        priceReductionDto.setEndDate(source.getEndDate());

        return priceReductionDto;
    }
}
