package com.formacion.practica.converters;

import com.formacion.practica.dto.ItemDto;
import com.formacion.practica.dto.PriceReductionDto;
import com.formacion.practica.dto.SupplierDto;
import com.formacion.practica.model.Item;
import com.formacion.practica.model.PriceReduction;
import com.formacion.practica.model.Supplier;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.List;

public class ItemConverter implements Converter<Item, ItemDto> {

    @Override
    public ItemDto convert(Item source) {
        ModelMapper modelMapper = new ModelMapper();
        ItemDto itemDto = modelMapper.map(source, ItemDto.class);
        return itemDto;
    }
}
