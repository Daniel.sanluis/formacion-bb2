package com.formacion.practica.dto;

import com.formacion.practica.enums.RoleEnum;
import lombok.Data;

@Data
public class RoleDto {

    private Long idRole;

    private RoleEnum role;

    private String description;
}
