package com.formacion.practica.model;

import com.formacion.practica.enums.RoleEnum;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="ROLE")
@Data
public class Role {

    @Id
    @Column(name = "IDROLE")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_generator")
    @SequenceGenerator(name="role_generator",initialValue = 6, allocationSize = 100)
    private Long idRole;

    @Column(name = "ROLE")
    private RoleEnum role;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToMany(cascade = CascadeType.PERSIST, mappedBy = "roles")
    private List<User> users;

}
