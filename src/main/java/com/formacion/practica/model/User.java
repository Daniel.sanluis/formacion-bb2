package com.formacion.practica.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="USER")
@Data
public class User {

    @Id
    @Column(name = "IDUSER")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
    @SequenceGenerator(name="user_generator",initialValue = 6, allocationSize = 100)
    private Long idUser;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "ENABLED")
    private boolean enabled;

    @ManyToMany
    private List<Role> roles;


}
