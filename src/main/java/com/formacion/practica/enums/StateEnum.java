package com.formacion.practica.enums;

public enum StateEnum {
    ACTIVE, DISCONTINUED;
}
