package com.formacion.practica.services;

import com.formacion.practica.dao.ItemDao;
import com.formacion.practica.dto.ItemDto;
import com.formacion.practica.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    ItemDao itemDao;

    @Autowired
    ConversionService conversionService;

    //Devuelve una lista de todos los artículos
    @Override
    public List<ItemDto> findAll() {
        List<Item> items = itemDao.findAll();
        List<ItemDto> itemsDto = items.stream()
                .map(e -> conversionService.convert(e, ItemDto.class))
                .collect(Collectors.toList());
        return itemsDto;
    }

    @Override
    public ItemDto findByIdItem(Long id) {
       return conversionService.convert(itemDao.findById(id),ItemDto.class) ;
    }

    @Override
    public boolean deleteItem(Long id) {
        if(itemDao.findById(id) != null){
            itemDao.deleteItem(id);
            return true;
        }
        return false;
    }

    @Override
    public boolean upadteItem(ItemDto itemDto) {
        if(itemDao.findById(itemDto.getIdItem()) != null){
            itemDao.updateItem(conversionService.convert(itemDto, Item.class));
            return true;
        }
        return false;
    }

    @Override
    public boolean saveItem(ItemDto itemDto) {
        if(itemDto.getIdItem() == null){
            itemDao.saveItem(conversionService.convert(itemDto, Item.class));
            return true;
        }
        return false;
    }
}
