package com.formacion.practica.services;

import com.formacion.practica.dto.UserDto;
import com.formacion.practica.model.User;

import java.util.List;

public interface UserService {
    public List<UserDto> findAll();
    public UserDto findByIdUser(Long id);
    public boolean deleteUser(Long id);
    public boolean upadteUser(User user);
    public boolean saveUser(User user);
}
