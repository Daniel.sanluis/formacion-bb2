package com.formacion.practica.services;

import com.formacion.practica.dao.UserDao;
import com.formacion.practica.dto.UserDto;
import com.formacion.practica.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Autowired
    ConversionService conversionService;

    //Devuelve una lista de todos los artículos
    @Override
    public List<UserDto> findAll() {
        List<User> users = userDao.findAll();
        List<UserDto> usersDto = users.stream()
                .map(e -> conversionService.convert(e, UserDto.class))
                .collect(Collectors.toList());
        return usersDto;
    }

    @Override
    public UserDto findByIdUser(Long id) {
        return conversionService.convert(userDao.findById(id),UserDto.class) ;
    }

    @Override
    public boolean deleteUser(Long id) {
        if(userDao.findById(id) != null){
            userDao.deleteUser(id);
            return true;
        }
        return false;
    }

    @Override
    public boolean upadteUser(User user) {
        if(userDao.findById(user.getIdUser()) != null){
            userDao.updateUser(user);
            return true;
        }
        return false;
    }

    @Override
    public boolean saveUser(User user) {
        if(user.getIdUser() == null){
            userDao.saveUser(user);
            return true;
        }
        return false;
    }
}
