package com.formacion.practica.controllers;

import com.formacion.practica.model.User;
import com.formacion.practica.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/findUsers")
    public ResponseEntity<Object> getUsers(HttpServletRequest request) throws  Exception{
        return new ResponseEntity<Object>(userService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/user/{idUser}")
    public ResponseEntity<Object> getUserById(HttpServletRequest request, @PathVariable Long idUser) throws Exception{
        return new ResponseEntity<Object>(userService.findByIdUser(idUser), HttpStatus.OK);
    }

    @DeleteMapping("/deleteUser/{idUser}")
    public boolean deleteUser(HttpServletRequest request, @PathVariable Long idUser) throws Exception{
        return userService.deleteUser(idUser);
    }

    @PostMapping("/saveUser")
    public boolean saveUser(HttpServletRequest request, @RequestBody User user) throws Exception{
        return userService.saveUser(user);
    }

    @PutMapping("/updateUser")
    public boolean updateUser(HttpServletRequest request, @RequestBody User user) throws Exception{
        return userService.upadteUser(user);
    }
}
