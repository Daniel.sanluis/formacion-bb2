package com.formacion.practica.controllers;

import com.formacion.practica.dto.ItemDto;
import com.formacion.practica.model.Item;
import com.formacion.practica.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class ItemController {

    @Autowired
    private ItemService itemService;

    @GetMapping("/findItems")
    public ResponseEntity<Object> getItems(HttpServletRequest request) throws  Exception{
        return new ResponseEntity<Object>(itemService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/item/{idItem}")
    public ResponseEntity<Object> getItemById(HttpServletRequest request, @PathVariable Long idItem) throws Exception{
        return new ResponseEntity<Object>(itemService.findByIdItem(idItem), HttpStatus.OK);
    }

    @DeleteMapping("/deleteItem/{idItem}")
    public boolean deleteItem(HttpServletRequest request, @PathVariable Long idItem) throws Exception{
        return itemService.deleteItem(idItem);
    }

    @PostMapping("/saveItem")
    public boolean saveItem(HttpServletRequest request, @RequestBody ItemDto item) throws Exception{
        return itemService.saveItem(item);
    }

    @PutMapping("/updateItem")
    public boolean updateItem(HttpServletRequest request, @RequestBody ItemDto item) throws Exception{
        return itemService.upadteItem(item);
    }

}
