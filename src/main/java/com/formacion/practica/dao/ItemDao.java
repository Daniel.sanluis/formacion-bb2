package com.formacion.practica.dao;

import com.formacion.practica.model.Item;

import java.util.List;

public interface ItemDao {
    public List<Item> findAll();
    public Item findById(Long id);
    public void deleteItem(Long id);
    public void updateItem(Item item);
    public void saveItem(Item item);
}
