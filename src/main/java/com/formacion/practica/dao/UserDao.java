package com.formacion.practica.dao;

import com.formacion.practica.model.User;

import java.util.List;

public interface UserDao {
    public List<User> findAll();
    public User findById(Long id);
    public void deleteUser(Long id);
    public void updateUser(User user);
    public void saveUser(User user);
}
