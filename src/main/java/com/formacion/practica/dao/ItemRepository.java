package com.formacion.practica.dao;

import com.formacion.practica.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;


@Repository
public interface ItemRepository extends JpaRepository<Item,Long> {

}
