
---------------------------ITEMS--------------------------------
insert into item values(1,0,'2020-02-05','Silla', 57897, 30.89);
insert into item values(2,0,'2020-08-26','Sillón', 51197, 30.89);
insert into item values(3,0,'2020-01-11','Mesa', 12358, 30.89);
insert into item values(4,1,'2020-06-08','Nevera', 87463, 30.89);
insert into item values(5,0,'2020-04-15','Mueble TV', 04710, 30.89);


---------------------------SUPPLIERS--------------------------------
insert into supplier values(1,'USA', 'TABLES JOHNSONS');
insert into supplier values(2,'SPAIN', 'MUEBLES JUAN');
insert into supplier values(3,'ITALY', ' STA.SL');
insert into supplier values(4,'GERMANY', 'LLQA');
insert into supplier values(5,'IRLAND', 'TABLES JACNSONS');


---------------------------ITEMS_SUPLIERS--------------------------------
insert into items_suppliers values(1,1);
insert into items_suppliers values(2,3);
insert into items_suppliers values(3,1);
insert into items_suppliers values(4,2);
insert into items_suppliers values(2,4);


---------------------------PRICEREDUCTIONS--------------------------------
insert into pricereduction values(1,'2020-05-10', 10.0,'2020-04-10', 1);
insert into pricereduction values(2,'2020-05-10', 20.0,'2020-04-10', 2);
insert into pricereduction values(3,'2020-06-01', 10.0,'2020-05-15', 3);
insert into pricereduction values(4,'2020-03-10', 10.0,'2020-01-10', 3);
insert into pricereduction values(5,'2020-08-11', 10.0,'2020-07-11', 5);


----------------------------ROLES--------------------------------
insert into role values(1,'GOD',0);
insert into role values(2,'HUMAN ',1);

----------------------------USERS--------------------------------
insert into user values(1,true, 'admin','admin');
insert into user values(2,true, 'user','user');

----------------------------USERS_ROLES--------------------------------
insert into user_roles values(1,1);
insert into user_roles values(1,2);
insert into user_roles values(2,2);